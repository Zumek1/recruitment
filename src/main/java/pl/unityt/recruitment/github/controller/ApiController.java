package pl.unityt.recruitment.github.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.unityt.recruitment.github.model.RepoInformationDto;
import pl.unityt.recruitment.github.service.GithubService;

@RestController
@RequestMapping("/repositories")
public class ApiController {
    @Autowired
    private GithubService githubService;

    @GetMapping(value = "/{owner}/{repository-name}", produces = "application/json")
    public ResponseEntity<RepoInformationDto> getRepoFromGithub(@PathVariable String owner, @PathVariable("repository-name") String repositoryName) {
        RepoInformationDto repoInformation = githubService.getGithubRepo(owner, repositoryName);
        return new ResponseEntity<>(repoInformation, HttpStatus.OK);
    }
}
