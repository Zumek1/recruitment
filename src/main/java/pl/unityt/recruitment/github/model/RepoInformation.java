package pl.unityt.recruitment.github.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonPropertyOrder({
        "full_name",
        "description",
        "clone_url",
        "watchers_count",
        "watchers_count",
        "created_at"
})
public class RepoInformation {
    @JsonProperty("full_name")
    private String fullName;
    @JsonProperty("description")
    private String description;
    @JsonProperty("clone_url")
    private String cloneUrl;
    @JsonProperty("watchers_count")
    private Long stars;
    @JsonProperty("created_at")
    private LocalDateTime createdAt;
}
