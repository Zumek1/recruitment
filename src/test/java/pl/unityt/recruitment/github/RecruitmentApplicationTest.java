package pl.unityt.recruitment.github;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import pl.unityt.recruitment.github.model.RepoInformationDto;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@EnableAutoConfiguration
public class RecruitmentApplicationTest {
    public static void main(String[] args) {
        SpringApplication.run(RecruitmentApplicationTest.class, args);
    }
    @Test
    void contextLoads() {
    }
    @Test
    void endpointTest(){
        RestTemplate restTemplate = new RestTemplate();
        String actual = restTemplate.getForObject("http://localhost:8080/repositories/Zumek1/CashFlow", RepoInformationDto.class).getFullName();
        Assert.assertEquals("Zumek1/CashFlow", actual );
    }

}
