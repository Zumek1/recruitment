package pl.unityt.recruitment.github.exception;

public class RepoNotFoundException extends RuntimeException {

    public RepoNotFoundException(String message) {
        super(message);
    }
}
