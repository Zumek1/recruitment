package pl.unityt.recruitment.github.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import pl.unityt.recruitment.github.exception.RepoNotFoundException;
import pl.unityt.recruitment.github.model.RepoInformation;
import pl.unityt.recruitment.github.model.RepoInformationDto;

@Service
public class GithubService {
    @Value("${service.url}")
    private String urlService;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private ModelMapper modelMapper;


    public RepoInformationDto getGithubRepo(String owner, String repoName) {
        String completeUrl = urlService + "/" + owner + "/" + repoName;
        try {
            RepoInformation repoInformation = restTemplate.getForObject(completeUrl, RepoInformation.class);
            RepoInformationDto repoInformationDto = modelMapper.map(repoInformation, RepoInformationDto.class);
            return repoInformationDto;
        } catch (HttpClientErrorException ex) {
            throw new RepoNotFoundException(completeUrl);
        }
    }
}

