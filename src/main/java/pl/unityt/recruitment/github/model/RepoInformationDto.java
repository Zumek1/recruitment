package pl.unityt.recruitment.github.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RepoInformationDto {
    private String fullName;
    private String description;
    private String cloneUrl;
    private Long stars;
    private LocalDateTime createdAt;
}
